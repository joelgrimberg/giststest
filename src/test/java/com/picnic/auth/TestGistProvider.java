package com.picnic.auth;

import javax.json.*;

class TestGistProvider {
    private TestGistProvider() {
    }

    public static JsonObject createGist(boolean isPublic) {

        return Json.createObjectBuilder()
                .add("description", "some description")
                .add("public", isPublic)
                .add("files", Json.createObjectBuilder()
                        .add("hello_world.rb", Json.createObjectBuilder()
                                .add("content", "some roebie content")))
                .build();
    }


    public static JsonObject updateGist() {
        return Json.createObjectBuilder()
                .add("description", "some updated description")
                .add("public", true)
                .add("files", Json.createObjectBuilder()
                        .add("hello_world.rb", Json.createObjectBuilder()
                                .add("content", "some roebie content"))
                        .add("hello_world.py", Json.createObjectBuilder()
                                .add("content", "some pai content"))
                )
                .build();
    }

}
