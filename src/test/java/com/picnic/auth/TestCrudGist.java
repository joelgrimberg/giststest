package com.picnic.auth;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import static io.restassured.RestAssured.given;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeAll;

import javax.json.JsonObject;

import java.util.List;
import java.net.URL;
import java.net.URLConnection;
import java.util.Map;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.*;



class TestCrudGist {

    private static RequestSpecification requestSpec;
    private static ResponseSpecification responseSpec;

    private final String GithubTokenWithGistScope = System.getenv("token");
    private static final String baseURI = "https://api.github.com/";

    @BeforeAll
    static void createRequestAndResponseSpecification() {
        requestSpec = new RequestSpecBuilder()
                .setBaseUri(baseURI)
                .build();

        responseSpec = new ResponseSpecBuilder()
                .expectContentType(ContentType.JSON)
                .build();
    }

    //Not Authorized Context: Fail to create a Gist
    @Test
    void notAuthorizedToCreateAGist() {
        JsonObject gist = TestGistProvider.createGist(true);
        given(requestSpec)
                .body(gist.toString())
                .post("gists")
                .then()
                .statusCode(401)
                .body("message", equalTo("Requires authentication"))
                .body("documentation_url", equalTo("https://developer.github.com/v3/gists/#create-a-gist"));
    }

    //Authorized Context: Create a Gist
    @Test
    void AuthorizedToCreateAGist() {
        JsonObject gist = TestGistProvider.createGist(false);
        given(requestSpec)
                .auth().oauth2(GithubTokenWithGistScope)
                .body(gist.toString())
                .post("gists")
                .then()
                .statusCode(201)
                .assertThat()
                .body("description", equalTo(gist.getString("description")))
                .body("public", equalTo(gist.getBoolean("public")))
                .body("files.'hello_world.rb'.content", equalTo(gist.get("files")
                        .asJsonObject().get("hello_world.rb").asJsonObject().getString("content")));
    }

    @Test
    void notAuthorizedToReadPrivateGist() {
        //first delete all gists
        deleteAllGists();

        //setup private gist
        JsonObject gist = TestGistProvider.createGist(false);

        String gistID =
                given(requestSpec)
                        .auth()
                        .oauth2(GithubTokenWithGistScope)
                        .when()
                        .body(gist.toString())
                        .post("gists")
                        .then()
                        .statusCode(201)
                        .extract()
                        .path("id");

        // private gists can be found via /gists/<gistID>.
        // to see if they cannot be found via the normal user-flow (without knowledge of their ID),
        // we have to check the user/gists with scope auth().none().
        given(requestSpec)
                .auth()
                .none()
                .when()
                .get(baseURI+ "users/joelgrimberg/gists")
                .then()
                .assertThat()
                .statusCode(403)
                .body("", empty());
    }

    @Test
    void AuthorizedToReadPrivateGist() {
        //setup private gist
        JsonObject gist = TestGistProvider.createGist(false);

        String gistID =
                given(requestSpec)
                        .auth()
                        .oauth2(GithubTokenWithGistScope)
                        .when()
                        .body(gist.toString())
                        .post("/gists")
                        .then()
                        .statusCode(201)
                        .extract()
                        .path("id");

        given(requestSpec)
                .auth().none()
                .when()
                .get("gists/" + gistID)
                .then()
                .assertThat()
                .statusCode(200)
                .contentType(ContentType.JSON);
    }

    @Test
    void notAuthorizedToUpdatePrivateGist() {
        JsonObject gist = TestGistProvider.createGist(true);
        JsonObject updatedGist = TestGistProvider.updateGist();

        //Insert New Gist
        String gistID =
                given(requestSpec)
                        .auth()
                        .oauth2(GithubTokenWithGistScope)
                        .when()
                        .body(gist.toString())
                        .post("gists")
                        .then()
                        .contentType(ContentType.JSON)
                        .statusCode(201)
                        .extract()
                        .path("id");

        given(requestSpec)
                .auth()
                .none()
                .body(updatedGist.toString())
                .post("gists/" + gistID )
                .then()
                .statusCode(404);
    }

    @Test
    void AuthorizedToUpdatePublicGist() {
        JsonObject gist = TestGistProvider.createGist(true);
        JsonObject updatedGist = TestGistProvider.updateGist();

        //Insert New Gist
        String gistID =
                given(requestSpec)
                        .auth()
                        .oauth2(GithubTokenWithGistScope)
                        .when()
                        .body(gist.toString())
                        .post("/gists")
                        .then()
                        .statusCode(201)
                        .extract()
                        .path("id");

                given(requestSpec)
                        .auth()
                        .oauth2(GithubTokenWithGistScope)
                        .body(updatedGist.toString())
                        .patch("gists/" + gistID);

        //Update Gist
        given(requestSpec)
                .auth()
                .oauth2(GithubTokenWithGistScope)
                .when()
                .get("gists/" + gistID)
                .then()
                .assertThat()
                .contentType(ContentType.JSON)
                .body("url", equalTo("https://api.github.com/gists/" + gistID))
                .body("description", equalTo(updatedGist.getString("description")))
                .body("public", equalTo(updatedGist.getBoolean("public")))
                .body("files.'hello_world.rb'.content", equalTo(updatedGist.get("files")
                        .asJsonObject().get("hello_world.rb").asJsonObject().getString("content")))
                .body("files.'hello_world.py'.content", equalTo(updatedGist.get("files")
                        .asJsonObject().get("hello_world.py").asJsonObject().getString("content")));
    }

    @Test
    void notAuthorizedToDeletePrivateGist() {
        JsonObject gist = TestGistProvider.createGist(false);

        //inserting Gist so it can be deleted
        String gistID =
                given(requestSpec)
                        .auth()
                        .oauth2(GithubTokenWithGistScope)
                        .body(gist.toString())
                        .post("gists")
                        .then()
                        .assertThat()
                        .statusCode(201)
                        .contentType(ContentType.JSON)
                        .body("description", equalTo(gist.getString("description")))
                        .body("public", equalTo(gist.getBoolean("public")))
                        .body("files.'hello_world.rb'.content", equalTo(gist.get("files").asJsonObject()
                                .get("hello_world.rb").asJsonObject().getString("content")))
                        .extract()
                        .path("id");

        // not authorized to delete a Gist
        given(requestSpec)
                .auth()
                .none()
                .delete("gists/"+gistID)
                .then()
                .statusCode(404);
    }

    @Test
    void AuthorizedToDeletePrivateGist() {
        JsonObject gist = TestGistProvider.createGist(false);

        //inserting Gist so it can be deleted
        String gistID =
                given(requestSpec)
                        .auth()
                        .oauth2(GithubTokenWithGistScope)
                        .body(gist.toString())
                        .post("gists")
                        .then()
                        .assertThat()
                        .statusCode(201)
                        .contentType(ContentType.JSON)
                        .body("description", equalTo(gist.getString("description")))
                        .body("public", equalTo(gist.getBoolean("public")))
                        .body("files.'hello_world.rb'.content", equalTo(gist.get("files").asJsonObject()
                                .get("hello_world.rb").asJsonObject().getString("content")))
                        .extract()
                        .path("id");

        // Deleting Gist -> deleting a gist results in a 204
        given(requestSpec)
                .auth()
                .oauth2(GithubTokenWithGistScope)
                .delete("gists/"+gistID)
                .then()
                .statusCode(204);

        // Again Deleting Gist. Will cause a 404 because Gist is already deleted
        given(requestSpec)
                .auth()
                .oauth2(GithubTokenWithGistScope)
                .when()
                .get("gists/" + gistID)
                .then()
                .assertThat()
                .contentType(ContentType.JSON)
                .statusCode(404);
    }

    // RateLimit for notAuthorized scope is set to 60 by Github.
    @Test
    public void notAuthorizedRateLimit() {
        JsonObject gist = TestGistProvider.createGist(false);

        String gistID =
                given(requestSpec)
                        .auth()
                        .oauth2(GithubTokenWithGistScope)
                        .body(gist.toString())
                        .post("gists")
                        .then()
                        .assertThat()
                        .statusCode(201)
                        .contentType(ContentType.JSON)
                        .body("description", equalTo(gist.getString("description")))
                        .body("public", equalTo(gist.getBoolean("public")))
                        .body("files.'hello_world.rb'.content", equalTo(gist.get("files").asJsonObject()
                                .get("hello_world.rb").asJsonObject().getString("content")))
                        .extract()
                        .path("id");

        try {
            URL obj = new URL(baseURI + "/gists/" + gistID);
            URLConnection conn = obj.openConnection();
            Map<String, List<String>> map = conn.getHeaderFields();

            int RateLimit =
                    given(requestSpec)
                            .auth()
                            .none()
                            .when()
                            .get(baseURI + "rate_limit")
                            .then()
                            .extract().path("resources.core.remaining");

            for(int i=0; i < RateLimit; i++) {
                given(requestSpec)
                        .auth()
                        .none()
                        .when()
                        .get(baseURI + "gists/" + gistID)
                        .then()
                        .assertThat()
                        .contentType(ContentType.JSON)
                        .statusCode(200);
            }

            // This one shows that you have reached the Rate Limit and therefore will gat a 403: Forbidden.
            given(requestSpec)
                    .auth()
                    .none()
                    .when()
                    .get(baseURI + "gists/" + gistID)
                    .then()
                    .assertThat()
                    .contentType(ContentType.JSON)
                    .statusCode(403);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // First Set the RateLimit, then fire the engines to deplete it and then get a 403 in Auth context.
    @Test
    public void AuthorizedRateLimit() {
        JsonObject gist = TestGistProvider.createGist(false);

        String gistID =
                given(requestSpec)
                        .auth()
                        .oauth2(GithubTokenWithGistScope)
                        .body(gist.toString())
                        .post("gists")
                        .then()
                        .assertThat()
                        .statusCode(201)
                        .contentType(ContentType.JSON)
                        .body("description", equalTo(gist.getString("description")))
                        .body("public", equalTo(gist.getBoolean("public")))
                        .body("files.'hello_world.rb'.content", equalTo(gist.get("files").asJsonObject()
                                .get("hello_world.rb").asJsonObject().getString("content")))
                        .extract()
                        .path("id");

        try {
            URL obj = new URL(baseURI + gistID );
            URLConnection conn = obj.openConnection();
            Map<String, List<String>> map = conn.getHeaderFields();

            int RateLimit =
            given(requestSpec)
                    .auth()
                    .oauth2(GithubTokenWithGistScope)
                    .when()
                    .get(baseURI + "rate_limit")
                    .then()
                    .extract().path("resources.core.remaining");

            List<String> XRatedLimit = map.get("X-RateLimit-Remaining");
            for(int i=0; i < RateLimit; i++) {
                given(requestSpec)
                        .auth()
                        .oauth2(GithubTokenWithGistScope)
                        .when()
                        .get(baseURI + "gists/" + gistID)
                        .then()
                        .assertThat()
                        .contentType(ContentType.JSON)
                        .statusCode(200);
            }

            // This one shows that you have reached the Rate Limit and therefore will gat a 403: Forbidden.
            given(requestSpec)
                    .auth()
                    .oauth2(GithubTokenWithGistScope)
                    .when()
                    .get(baseURI + "gists/" + gistID)
                    .then()
                    .assertThat()
                    .contentType(ContentType.JSON)
                    .statusCode(403);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteAllGists() {
        for (int i = 1; i < 30; i++) {
            List<String> gistID =
                    given(requestSpec)
                            .auth()
                            .oauth2(GithubTokenWithGistScope)
                            .get("users/joelgrimberg/gists?page=1&per_page=3000")
                            .then()
                            .extract()
                            .path("id");

            for(int y=0; y< gistID.size(); y++){
                given(requestSpec)
                        .auth()
                        .oauth2(GithubTokenWithGistScope)
                        .delete("gists/" + gistID.get(y));
           }
        }

    }
}




