# Gist Tests

## Requires
- Java 11

## Running tests from Intellij on macOS:
!! Make sure you run the rate limiting tests at the end. Because else the CRUD tests will fail.
Also make sure you have your Gists token setup as environment variable before running the tests:
1. On Terminal: export token= < your gists token >
2. Run Intellij from that shell with the command 'idea'
    (if this command is not set, go to tools and start 'Create Command-line Launcher') and quit your Intellij. 
    Then try 'idea' gain
3. Run tests, et voila!

## About Rate Limiting
Because the testsObject is an object that is not under my control, 
I cannot set it up in a proper way to test the rate limit mechanism.
I would like to be able to whitelist my IP and to reset my Rate-Limit timer by hand (or code, so to speak)
Testcases I would test:
1. after hitting the api one time, would it X-RateLimit-Remaining -1 ?
2. does whitelisting work
3. does blacklisting work
4. 1 User with 2 tokens: would both hits do a -1 on the same User X-RateLimit-Remaining ?


  




